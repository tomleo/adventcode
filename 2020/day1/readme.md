# Part 1

you'll need to find fifty of these coins by the time you arrive so you can pay
the deposit on your room.

find the two entries that sum to 2020 and then multiply those two numbers
together.

## Idea

```
0 1
0 2
0 3
...
0 N
1 0
1 1 (skip)
1 2
...
1 N
```

```python
inputs = [int(i) for i in puts.decode('utf8').split('\n') if i]
inputs = [1721, 979, 366, 299, 675, 1456]

matches = []
index = 0

for i in range(len(inputs)):
    for j in range(len(inputs)):
        if i == j:
            continue
        lower_bound = inputs[i]
        upper_bound = inputs[j]
        loc_sum = lower_bound + upper_bound
        if loc_sum == 2020:
            matches.append((i, j))

print("Matches: ")
for match in matches:
    print("inputs[%s]: %s, inputs[%s]: %s" % (
        match[0], inputs[match[0]],
        match[1], inputs[match[1]]
    ))

firstMatch = matches[0]
print(f"finalResult: {inputs[firstMatch[0]] * inputs[firstMatch[1]]}")
```

## Part 2

```python
matches = []

for i in range(len(inputs)):
    for j in range(len(inputs)):
        if i == j:
            continue
        for k in range(len(inputs)):
            if i == k:
                continue
            if j == k:
                continue

            lower_bound = inputs[i]
            midds_bound = inputs[j]
            upper_bound = inputs[k]

            loc_sum = lower_bound + midds_bound + upper_bound
            if loc_sum == 2020:
                matches.append((i, j, k))

match = matches[0]
product = inputs[match[0]] * inputs[match[1]] * inputs[match[2]]
print("product: {product}")
```


