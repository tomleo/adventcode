from pprint import pprint
from operator import mul
from functools import reduce

def map_to_str(m_list):
    result = [
        "".join(row)
        for row in m_list
    ]
    return "\n".join(result)

def make_map(m_str, max_x=1, max_y=1):
    """
    m is the initial map "shape"
    """
    m = []
    for row in m_str.split('\n'):
        if not row:
            continue
        m.append([i for i in row.strip() if i])
    
    if max_x >= len(m[0]):
        amount_to_increase = int(max_x / len(m[0]))
        new_m = []
        for row in m:
            new_row = []
            for i in range(amount_to_increase+1):
                new_row += row
            new_m.append(new_row)
        m = new_m

    if max_y >= len(m):
        new_m = []
        for i in range(2):
            for row in m:
                new_m.append(row)
        m = new_m
          
    return m

def traverse_map(m_str, x_movement=3, y_movement=1):
    x = 0
    y = 0
    path = []
    m = make_map(m_str)
    for i in range(len(m)):
        y += y_movement
        x += x_movement

        try:
            m[y]
        except IndexError:
            return path
        
        try:
            value = m[y][x]
        except IndexError:
            m = make_map(m_str, x)
            value = m[y][x]

        path.append(
            {'x': x, 'y': y, 'value': value}
        )
    return path

if __name__ == '__main__':
    test_input = """..##.......
    #...#...#..
    .#....#..#.
    ..#.#...#.#
    .#...##..#.
    ..#.##.....
    .#.#.#....#
    .#........#
    #.##...#...
    #...##....#
    .#..#...#.#"""

    with open('input', 'rb') as fin:
        test_input = fin.read().decode('utf-8')

    tree_counts = []
    traverse_patterns = [
        (1, 1),
        (3, 1),
        (5, 1),
        (7, 1),
        (1, 2),
    ]
    for traverse in traverse_patterns:
        tree_count = 0
        path = traverse_map(test_input, *traverse)
        for p in path:
            if p['value'] == '#':
                tree_count += 1
        print(tree_count)
        tree_counts.append(tree_count)

    print(reduce(mul, tree_counts))

